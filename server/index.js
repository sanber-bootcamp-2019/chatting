const { ApolloServer, gql } = require("apollo-server");
const fs = require("fs");
const { resolvers } = require("./resolvers");
const { context } = require("./context");

// read schema
const typeDefs = gql(fs.readFileSync("./schema.gql", "utf8"));

// create a simple server that resolve
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context,
  tracing: false
});

const PORT = process.env.PORT || 3000;

// launch server
server.listen(PORT).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
