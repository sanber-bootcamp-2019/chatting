const { profile } = require("./resolvers/profile");
const { messages } = require("./resolvers/messages");
const { Message } = require("./resolvers/message");
const { sendMessage } = require("./resolvers/send-message");
const { register } = require("./resolvers/register");
const { login } = require("./resolvers/login");
const { messageSent } = require("./resolvers/message-sent");

const resolvers = {
  Query: {
    profile,
    messages
  },
  Mutation: {
    sendMessage,
    login,
    register
  },
  Subscription: {
    messageSent
  },
  Message
};

exports.resolvers = resolvers;
