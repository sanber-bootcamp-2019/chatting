const { db } = require("./db");
const { getUsers, getUserByID, createUser } = require("./user");

describe("User Management", () => {
  const users = [
    { name: "jhon doe" },
    { name: "jane doe" },
    { name: "budi setiawan" }
  ];

  beforeEach(async () => {
    await db.del("users");
  });

  describe(".getUsers", () => {
    it("should return list of users", async () => {
      await db.put("users", JSON.stringify(users));
      const results = await getUsers();
      expect(results).toMatchObject(users);
    });

    it("should return empty array when db not exists or empty", async () => {
      const r1 = await getUsers();
      expect(r1.length).toBe(0);
      await db.put("users", JSON.stringify([]));
      const r2 = await getUsers();
      expect(r2.length).toBe(0);
    });
  });

  describe(".getUserByID", () => {
    it("should return user by it's index number", async () => {
      await db.put("users", JSON.stringify(users));
      const u1 = await getUserByID(0);
      expect(u1.name).toBe("jhon doe");
      const u2 = await getUserByID(2);
      expect(u2.name).toBe("budi setiawan");
    });
    it("should return null when user not found", async () => {
      const u1 = await getUserByID(4);
      expect(u1).toBeNull();
      await db.put("users", JSON.stringify(users));
      const u2 = await getUserByID(12);
      expect(u2).toBeNull();
    });
  });

  describe(".createUser", () => {
    it("should create new user", async () => {
      await db.put("users", JSON.stringify(users));
      await createUser("anak baru");
      const r1 = await getUsers();
      expect(r1.length).toBe(4);
    });
    it("should return new user id", async () => {
      await db.put("users", JSON.stringify(users));
      const id = await createUser("anak baru");
      const user = await getUserByID(id);
      expect(user.name).toBe("anak baru");
    });
    it("should throw error when name already exist", async () => {
      await db.put("users", JSON.stringify(users));
      try {
        await createUser("budi setiawan");
      } catch (err) {
        expect(err).toEqual(new Error("user already exists"));
      }
    });
  });
});
