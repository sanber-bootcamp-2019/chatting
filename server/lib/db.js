const levelup = require("levelup");
const leveldown = require("leveldown");
const path = require("path");

const storeLocation = path.resolve(__dirname, "../chatting.db");
const db = levelup(leveldown(storeLocation));

exports.db = db;
exports.storeLocation = storeLocation;
