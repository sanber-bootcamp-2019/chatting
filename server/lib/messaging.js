const { db } = require("./db");

async function getMessages() {
  try {
    const val = await db.get("messages");
    return JSON.parse(val);
  } catch (err) {
    // failed to fetch messages
    return [];
  }
}

async function getMessageByID(id) {
  const messages = await getMessages();
  return messages[id] || null;
}

async function saveMessage(author, content) {
  const messages = await getMessages();
  const message = { author, content };
  messages.push(message);
  await db.put("messages", JSON.stringify(messages));
  return { ...message, id: messages.length - 1 };
}

exports.getMessages = getMessages;
exports.getMessageByID = getMessageByID;
exports.saveMessage = saveMessage;
