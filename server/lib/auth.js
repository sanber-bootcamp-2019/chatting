const { getUserByName } = require("./user");
const jwt = require("jsonwebtoken");

const JWT_SECRET = "server-key-secret";

async function createToken(name) {
  const user = await getUserByName(name);
  if (!user) {
    throw new Error("user not found!");
  }
  // put id information on user
  const token = jwt.sign(user, JWT_SECRET);
  return token;
}

async function verifyToken(token) {
  const user = jwt.verify(token, JWT_SECRET);
  if (!user) {
    throw new Error("token not valid");
  }
  return user;
}

exports.createToken = createToken;
exports.verifyToken = verifyToken;
exports.JWT_SECRET = JWT_SECRET;
