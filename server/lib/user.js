const { db } = require("./db");

async function getUsers() {
  try {
    const val = await db.get("users");
    return JSON.parse(val);
  } catch (err) {
    // failed to fetch users
    return [];
  }
}

async function getUserByID(id) {
  const users = await getUsers();
  const user = users[id];
  if (!user) {
    return null;
  }
  return { ...user, id };
}

async function getUserByName(name) {
  const users = await getUsers();
  const id = users.findIndex(u => u.name === name);
  if (id === -1) {
    return null;
  }
  return { name, id };
}

async function createUser(name) {
  const users = await getUsers();
  if (users.find(u => u.name === name)) {
    throw new Error("user already exists");
  }
  users.push({ name });
  await db.put("users", JSON.stringify(users));
  return users.length - 1;
}

exports.getUsers = getUsers;
exports.getUserByID = getUserByID;
exports.getUserByName = getUserByName;
exports.createUser = createUser;
