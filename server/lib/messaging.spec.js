const { db } = require("./db");
const { getMessages, getMessageByID } = require("./messaging");

describe.skip("Messaging", () => {
  const messages = [
    { author: 1, content: "first message" },
    { author: 1, content: "another message" },
    { author: 2, content: "reply from 2" }
  ];

  beforeEach(async () => {
    await db.del("messages");
  });

  describe(".getMessages", () => {
    it("should return list of messages", async () => {
      await db.put("messages", JSON.stringify(messages));
      const results = await getMessages();
      expect(results).toMatchObject(messages);
    });

    it("should return empty array when db not exists or empty", async () => {
      const r1 = await getMessages();
      expect(r1.length).toBe(0);
      await db.put("messages", JSON.stringify([]));
      const r2 = await getMessages();
      expect(r2.length).toBe(0);
    });
  });

  describe(".getMessageByID", () => {
    it("should return message by it's index number", async () => {
      await db.put("messages", JSON.stringify(messages));
      const m1 = await getMessageByID(0);
      expect(m1.author).toBe(1);
      expect(m1.content).toBe("first message");
      const m2 = await getMessageByID(2);
      expect(m2.author).toBe(2);
      expect(m2.content).toBe("reply from 2");
    });
    it("should return null when message not found", async () => {
      const m1 = await getMessageByID(4);
      expect(m1).toBeNull();
      await db.put("messages", JSON.stringify(messages));
      const m2 = await getMessageByID(12);
      expect(m2).toBeNull();
    });
  });
});
