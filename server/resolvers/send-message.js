const { checkAuthentication } = require("./utils/require-auth");
const { pubsub } = require("./utils/pubsub");
const { saveMessage } = require("../lib/messaging");

async function sendMessage(root, { content }, ctx) {
  const ok = checkAuthentication(ctx);
  if (!ok) {
    return;
  }
  const message = await saveMessage(ctx.user.id, content);
  await pubsub.publish("message-sent", { messageSent: message });
  return message;
}

exports.sendMessage = sendMessage;
