const { checkAuthentication } = require("./utils/require-auth");
const { getMessages } = require("../lib/messaging");

async function messages(root, args, ctx) {
  const ok = checkAuthentication(ctx);
  if (!ok) {
    return;
  }
  const messages = await getMessages();
  return messages.map((message, id) => ({
    ...message,
    id
  }));
}

exports.messages = messages;
