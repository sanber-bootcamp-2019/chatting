const { checkAuthentication } = require("./utils/require-auth");

async function profile(root, args, ctx) {
  const ok = checkAuthentication(ctx);
  if (!ok) {
    return;
  }

  return ctx.user;
}

exports.profile = profile
