const { createToken } = require("../lib/auth");

async function login(root, { name }, ctx) {
  const token = await createToken(name);
  return token;
}

exports.login = login;
