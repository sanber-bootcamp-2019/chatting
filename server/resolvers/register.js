const { createUser } = require("../lib/user");

async function register(root, { name }, ctx) {
  const id = await createUser(name);
  return { id, name };
}

exports.register = register;
