const { checkAuthentication } = require("./utils/require-auth");
const { getUserByID } = require("../lib/user");

const Message = {
  mine(root, args, ctx) {
    const ok = checkAuthentication(ctx);
    if (!ok) {
      return;
    }
    return ctx.user.id === root.author;
  },
  async sender(root, args, ctx) {
    const user = await getUserByID(root.author);
    if (!user) {
      throw new Error("user not found!");
    }
    return user;
  }
};

exports.Message = Message;
