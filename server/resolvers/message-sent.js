const { checkAuthentication } = require("./utils/require-auth");
const { pubsub } = require("./utils/pubsub");

const messageSent = {
  subscribe(root, args, ctx) {
    const ok = checkAuthentication(ctx);
    if (!ok) {
      return;
    }
    return pubsub.asyncIterator(["message-sent"]);
  }
};

exports.messageSent = messageSent;
