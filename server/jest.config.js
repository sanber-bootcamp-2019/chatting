module.exports = {
  verbose: true,
  moduleFileExtensions: ["js"],
  testMatch: ["<rootDir>/lib/**/*.spec.js"],
  collectCoverage: true,
  collectCoverageFrom: ["<rootDir>/lib/"],
  coverageDirectory: "<rootDir>/coverage",
  coverageReporters: ["html", "text-summary", "text"],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80
    }
  }
};
