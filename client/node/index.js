const rp = require("request-promise");

const api = "http://localhost:3000/";

function getProfile(token) {
  return rp(api, {
    headers: {
      "authorization": token
    },
    method: "POST",
    json: true,
    body: {
      query: `{ profile { name } }`,
      variables: {},
      operationName: ""
    }
  });
}

function login(username) {
  return rp(api, {
    method: "POST",
    json: true,
    body: {
      query: `
        mutation masuk($username: String!) {
          login(name: $username)
        }
      `,
      variables: { username },
      operationName: "masuk"
    }
  })
}

async function main(username) {
  const info = await login(username);
  if (info.errors) {
    console.log(JSON.stringify(info.errors, null, 2));
    return;
  }
  const token = info.data.login;
  const profile = await getProfile(token);
  console.log(profile);
}

main("tidak dikenal");