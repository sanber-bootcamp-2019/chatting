const { ApolloClient } = require("apollo-client");
const { InMemoryCache } = require("apollo-cache-inmemory");
const { split } = require("apollo-link");
const { WebSocketLink } = require("apollo-link-ws");
const { SubscriptionClient } = require("subscriptions-transport-ws");
const { getMainDefinition } = require("apollo-utilities");
const { setContext } = require("apollo-link-context");
const { HttpLink } = require("apollo-link-http");
const loginMutation = require("./login.gql");
const getMessagesQuery = require("./messages.gql");
const messageSentSubscriptions = require("./message-sent.gql");

let token;
let messages;

const cache = new InMemoryCache();
const httpLink = new HttpLink({
  uri: "http://localhost:3000"
});
const authLink = setContext((_, { headers }) => {
  return {
    headers: {
      ...headers,
      authorization: token ? token : ""
    }
  };
});

const websocketClient = new SubscriptionClient(
  'ws://localhost:3000/graphql',
  {
    reconnect: true
  }
);
websocketClient.use([{
  async applyMiddleware(options, next) {
    options.authorization = token ? token : "";
    next();
  }
}]);
const wsLink = new WebSocketLink(websocketClient);

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' &&
      operation === 'subscription';
  },
  wsLink,
  authLink.concat(httpLink)
)

const client = new ApolloClient({
  link,
  cache
});

openLoginPage();

async function loadMessages() {
  // query messages & profile
  const raw = await client.query({
    query: getMessagesQuery
  });
  messages = raw.data.messages;

  // subscribe message sent
  subscribeMessageSent();

  renderHeader(raw.data.profile.name);
  renderChat(messages);
}

async function login(event) {
  event.preventDefault();
  const username = document.getElementById("username").value;

  // get token from server
  const raw = await client.mutate({
    mutation: loginMutation,
    variables: { username }
  });
  token = raw.data.login;
  if (token) {
    history.pushState({ username }, 'chatPage', '#chat');
    openChatPage();
  }

  // load messages
  loadMessages();
}
window.login = login;

function renderChat(messages) {
  for (let msg of messages) {
    insertChat(msg.sender.name, msg.content);
  }
}

function insertChat(author, message) {
  const messageList = document.getElementById("message-list");
  const chat = renderMessageBox(author, message);
  messageList.append(chat);
}

function resetChat() {
  const messageList = document.getElementById("message-list");
  messageList.innerHTML = "";
}

function renderMessageBox(author, message) {
  const messageBox = document.createElement("div");
  const authorBox = document.createElement("h4");
  authorBox.innerText = author;
  const contentBox = document.createElement("p");
  contentBox.innerText = message;
  messageBox.append(authorBox);
  messageBox.append(contentBox);
  return messageBox;
}

function renderHeader(name) {
  const myProfile = document.getElementById("my-profile");
  myProfile.innerText = name;
}

async function subscribeMessageSent() {
  const chatSubs = await client.subscribe({
    query: messageSentSubscriptions
  });

  chatSubs.subscribe((raw) => {
    console.log(raw);
    const message = raw.data.messageSent;
    const found = messages.find(
      m => m.id === message.id
    );
    if (!found) {
      messages.push(message);
      insertChat(
        message.sender.name,
        message.content
      );
    }
  });
}

async function sendChat(event) {
  event.preventDefault();
  const content = document.getElementById("chat");
  try {
    if (!token) {
      return;
    }
    const result = await fetch("http://localhost:3000/", {
      method: "POST",
      headers: {
        "content-type": "application/json",
        authorization: token
      },
      body: JSON.stringify({
        query: `mutation kirimPesan($content: String!){
          sendMessage(content: $content) {
            id
            content
            mine
            sender { name }
          }
        }`,
        variables: { content: content.value },
        operationName: "kirimPesan"
      })
    });
    const raw = await result.json();
    const message = raw.data.sendMessage;
    const found = messages.find(m => m.id === message.id);
    if (!found) {
      messages.push(message);
      insertChat("budi setiawan", message.content);
    }

    history.pushState({ messages }, "sendChat", "#chat");
    // reset after succesffuly sent message
    content.value = "";
  } catch (err) {
    console.error(err);
  }
}
window.sendChat = sendChat;

function openChatPage() {
  document.getElementById('login-form').style.display = 'none';
  document.getElementById('chat-component').style.display = 'block';
}

function openLoginPage() {
  document.getElementById('login-form').style.display = 'block';
  document.getElementById('chat-component').style.display = 'none';
}

window.addEventListener('popstate', (event) => {
  const state = event.state;
  if (state) {
    if (state.username) {
      openLoginPage();
      document.getElementById("username").value = state.username;
    }
    if (state.messages) {
      openChatPage();
      resetChat();
      renderChat(state.messages);
    }
  }
});
